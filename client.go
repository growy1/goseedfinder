package goseedfinder

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
)

// Client is the seedfinder api client
type Client struct {
	BaseURL    string
	APIKey     string
	HTTPClient *http.Client
}

// New init a new client with apikey
func New(apiKey string) Client {
	return Client{
		APIKey:     apiKey,
		HTTPClient: http.DefaultClient,
		BaseURL:    "http://en.seedfinder.eu/api/json",
	}
}

// StrainSearch for a strain id
func (c *Client) StrainSearch(query string) (*Search, error) {
	var result Search
	queryURL := fmt.Sprintf("%s/search.json?ac=%s&q=%s",
		c.BaseURL,
		c.APIKey,
		url.QueryEscape(query))
	req, err := http.NewRequest(http.MethodGet, queryURL, nil)
	if err != nil {
		return nil, err
	}
	res, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	if err != nil {
		fmt.Printf("Error(Search): %s\n", err.Error())
		return nil, err
	}
	if res.StatusCode == http.StatusForbidden {
		fmt.Printf("Error(Search): Got status %v\n", res.StatusCode)
		return nil, errors.New("bad credentials")
	} else if res.StatusCode != http.StatusOK {
		fmt.Printf("Error(Search): Got status %v\n", res.StatusCode)
		return nil, errors.New("api error")
	}
	dec := json.NewDecoder(res.Body)
	dec.DisallowUnknownFields()
	if err = dec.Decode(&result); err != nil {
		return nil, err
	}
	return &result, err
}

// StrainInfo for a particular strain
func (c *Client) StrainInfo(strainID, breederID string) (*StrainInfo, error) {
	var result StrainInfo
	queryURL := fmt.Sprintf("%s/strain.json?ac=%s&br=%s&str=%s",
		c.BaseURL,
		c.APIKey,
		breederID,
		strainID)
	req, err := http.NewRequest(http.MethodGet, queryURL, nil)
	if err != nil {
		return nil, err
	}
	res, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	if err != nil {
		fmt.Printf("Error(Search): %s\n", err.Error())
		return nil, err
	}
	if res.StatusCode == http.StatusForbidden {
		fmt.Printf("Error(Search): Got status %v\n", res.StatusCode)
		return nil, errors.New("bad credentials")
	} else if res.StatusCode != http.StatusOK {
		fmt.Printf("Error(Search): Got status %v\n", res.StatusCode)
		return nil, errors.New("api error")
	}
	dec := json.NewDecoder(res.Body)
	dec.DisallowUnknownFields()
	if err = dec.Decode(&result); err != nil {
		return nil, err
	}
	return &result, err
}
