package goseedfinder

// FloweringInfo is the estimated flowering time for a strain
type FloweringInfo struct {
	Auto bool   `json:"auto"`
	Days int    `json:"days"`
	Info string `json:"info"`
}

// BreederInfo is the breeder info on strain
type BreederInfo struct {
	Name        string        `json:"name"`
	ID          string        `json:"id"`
	Type        string        `json:"type"`
	CBD         string        `json:"cbd"`
	Picture     string        `json:"pic"`
	Flowering   FloweringInfo `json:"flowering"`
	Description string        `json:"descr"`
}

// UploadLinks to add content for a strain
type UploadLinks struct {
	Picture string `json:"picture"`
	Review  string `json:"review"`
	Medical string `json:"medical"`
}

// Links contain some links back to the seedfinder like the link directly to the strain-info, to the review and also some links to our upload-functions.
type Links struct {
	Info   string      `json:"info"`
	Review string      `json:"review"`
	Upload UploadLinks `json:"upload"`
}

// Licence is the licence infos
type Licence struct {
	URLCreativeCommons string `json:"url_cc"`
	URLSeedFinder      string `json:"url_sf"`
	Info               string `json:"info"`
}

// StrainInfo is an object containing the requested details for the variety, some upload-links and licenses.
type StrainInfo struct {
	Error       bool        `json:"error"`
	Name        string      `json:"name"`
	ID          string      `json:"id"`
	BreederInfo BreederInfo `json:"brinfo"`
	Comments    bool        `json:"comments"`
	Links       Links       `json:"links"`
	Licence     Licence     `json:"licence"`
}

// Strains is the results of a strain search
type Strains map[string]struct {
	Name        string `json:"name"`
	BreederName string `json:"brname"`
	ID          string `json:"id"`
	BreederID   string `json:"brid"`
}

// Search is a search result
type Search struct {
	Error   bool    `json:"error"`
	Count   int     `json:"count"`
	Info    string  `json:"info"`
	Strains Strains `json:"strains"`
}
